**● SpringBoot mybatis paging**
---
### 1. pom.xml에  라이브러리 설정
~~~
<dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-jdbc</artifactId>
        </dependency>

        <dependency>
            <groupId>org.mybatis.spring.boot</groupId>
            <artifactId>mybatis-spring-boot-starter</artifactId>
            <version>2.1.0</version>
        </dependency>
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>8.0.17</version>
        </dependency>

        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <optional>true</optional>
        </dependency>
~~~
### 2. application.properties 설정 
~~~
#server
server.port=8080

#database
#spring.datasource.url=jdbc:mysql://localhost:3306/mybatis_paging?serverTimezone=UTC
spring.datasource.url=jdbc:mysql://localhost:3306/mybatis_paging?serverTimezone=Asia/Seoul
spring.datasource.username=myborn
spring.datasource.password=password
spring.datasource.driver-class-name=com.mysql.cj.jdbc.Driver

#hikari
spring.datasource.hikari.connectionTimeout=30000
spring.datasource.hikari.idleTimeout=600000
spring.datasource.hikari.maxLifetime=1800000

#mybatis
mybatis.mapper-locations=classpath:mapper/*.xml
~~~

### 3. config설정을 통해 mapperScan  설정
~~~
@SpringBootApplication
@MapperScan("com.suby.springbootmybatispaging.mapper")
~~~

### 4. controller 추가
### 5. dto 추가
### 6. mapper 추가
### 7. Mybatis-PageHelper 라이브러리 이용해서 페이징 처리
#### 7-1. 라이브러리 추가
~~~
<dependency>
    <groupId>com.github.pagehelper</groupId>
    <artifactId>pagehelper-spring-boot-starter</artifactId>
    <version>1.2.12</version>
</dependency>
~~~

#### 7-2 mybatis pagehelper 설정
~~~
- helperDialect : 사용할 DB, (oracle, mysql, mariadb, sqlite, hsqldb, postgresql, db2, sqlserver, informix, h2, sqlserver2012, derby)
- reasonable : 기본 값은 false 이다. true로 설정하게 되면 pageNum <= 0 경우  첫 번째 페이지, pageNum > pages 마지막 페이지보다 크더라도 마지막 페이지이다.  
- suportMethodsArgumetns : 기본 값은 false 이다. true로 설정하게 되면 Mapper에서 page parameter를 사용할 수 있다. 
~~~

#### 7-3 pahehelper 호출
~~~
PageHelper.startPage(1, 10);
Page<Member> members = this.mybatisPagingMapper.selectMember();
PageInfo pageResult = new PageInfo(members);

or lambda 이용
PageInfo<Member> pageInfo = PageHelper.startPage(pageNum, 10).doSelectPageInfo(() -> this.mybatisPagingMapper.selectMember());
~~~

## 참고 url
https://github.com/libedi/spring-web-sample


https://www.programcreek.com/java-api-examples/?api=com.github.pagehelper.PageInfo