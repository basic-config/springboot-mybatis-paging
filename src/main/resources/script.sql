CREATE TABLE `member` (
  `id` varchar(25) COLLATE utf8_bin NOT NULL,
  `password` varchar(25) COLLATE utf8_bin NOT NULL,
  `name` varchar(25) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `email` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

insert into member( id,password,name,email ) values( 'user1', 'password', '사용자1', 'user1@mail.com');
insert into member( id,password,name,email ) values( 'user2', 'password', '사용자2', 'user2@mail.com');
insert into member( id,password,name,email ) values( 'user3', 'password', '사용자3', 'user3@mail.com');
insert into member( id,password,name,email ) values( 'user4', 'password', '사용자4', 'user4@mail.com');
insert into member( id,password,name,email ) values( 'user5', 'password', '사용자5', 'user5@mail.com');
insert into member( id,password,name,email ) values( 'user6', 'password', '사용자6', 'user6@mail.com');
insert into member( id,password,name,email ) values( 'user7', 'password', '사용자7', 'user7@mail.com');
insert into member( id,password,name,email ) values( 'user8', 'password', '사용자8', 'user8@mail.com');
insert into member( id,password,name,email ) values( 'user9', 'password', '사용자9', 'user9@mail.com');
insert into member( id,password,name,email ) values( 'user10', 'password', '사용자10', 'user10@mail.com');
insert into member( id,password,name,email ) values( 'user11', 'password', '사용자11', 'user11@mail.com');
insert into member( id,password,name,email ) values( 'user12', 'password', '사용자12', 'user12@mail.com');
insert into member( id,password,name,email ) values( 'user13', 'password', '사용자13', 'user13@mail.com');
insert into member( id,password,name,email ) values( 'user14', 'password', '사용자14', 'user14@mail.com');
insert into member( id,password,name,email ) values( 'user15', 'password', '사용자15', 'user15@mail.com');
insert into member( id,password,name,email ) values( 'user16', 'password', '사용자16', 'user16@mail.com');
insert into member( id,password,name,email ) values( 'user17', 'password', '사용자17', 'user17@mail.com');
insert into member( id,password,name,email ) values( 'user18', 'password', '사용자18', 'user18@mail.com');
insert into member( id,password,name,email ) values( 'user19', 'password', '사용자19', 'user19@mail.com');
insert into member( id,password,name,email ) values( 'user20', 'password', '사용자20', 'user20@mail.com');
insert into member( id,password,name,email ) values( 'user21', 'password', '사용자21', 'user21@mail.com');
