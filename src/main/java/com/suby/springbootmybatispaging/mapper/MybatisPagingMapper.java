package com.suby.springbootmybatispaging.mapper;

import com.github.pagehelper.PageInfo;
import com.suby.springbootmybatispaging.dto.Member;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface MybatisPagingMapper {
    List<Member> selectMember();
}
