package com.suby.springbootmybatispaging.controller;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.suby.springbootmybatispaging.dto.Member;
import com.suby.springbootmybatispaging.mapper.MybatisPagingMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/mybatis")
public class MybatisPagingController {
    private final MybatisPagingMapper mybatisPagingMapper;

    @GetMapping("/paging/{pageNum}")
    public PageInfo<Member> mybatisPaging(@PathVariable("pageNum") int pageNum){
//        PageHelper.startPage(pageNum, 10);
//        List<Member> members = this.mybatisPagingMapper.selectMember();
//        PageInfo pageResult = new PageInfo(members);

        PageInfo<Member> pageInfo = PageHelper.startPage(pageNum, 10).doSelectPageInfo(() -> this.mybatisPagingMapper.selectMember());
        return pageInfo;
    }
}

