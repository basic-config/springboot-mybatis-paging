package com.suby.springbootmybatispaging;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.suby.springbootmybatispaging.mapper")
public class SpringbootMybatisPagingApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootMybatisPagingApplication.class, args);
    }

}
